/*!
 * \file    parameters.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Program parameters and handling command-line arguments
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef PARAMETERS_H_INCLUDED
#define PARAMETERS_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <stdint.h>
#include <limits.h>
#include <math.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

/* ****************************************************************************
*           GLOBAL VARIABLES - Parameters
******************************************************************************/

uint64_t    param_swarm_size        = 50000;    /*!< \brief  Swarm size */

float       param_swarm_cognitive   = 1.494f;   /*!< \brief  Cognitive coefficient */
float       param_swarm_social      = 1.494f;   /*!< \brief  Social coefficient */
float       param_swarm_inertia     = 0.729f;   /*!< \brief  Inertial coefficient */

float       param_swarm_vmax        = 0.005f;   /*!< \brief  Maximal velocity */
float       param_swarm_dt          = 1.0f;     /*!< \brief  Time step */

const v3D(float)  param_swarm_min_bound   = {-1.0, -1.0, -1.0};   /*!< \brief Minimal coordinates of optimization space - constant*/
const v3D(float)  param_swarm_max_bound   = {+1.0, +1.0, +1.0};   /*!< \brief Maximal coordinates of optimization space - constant*/

int8_t      param_swarm_same_rand   = 0;        /*!< \brief  Should inertial coefficient be multiplied by random number from (0,1) range? */
int8_t      param_swarm_randw       = 0;        /*!< \brief  Should all random numbers used in one update call be the same? */
int8_t      param_swarm_kb          = 0;        /*!< \brief  Should function minimum be given to the swarm (tracking)? */

int8_t      param_force_FPS         = 0;        /*!< \brief  Should program force FPS to maximum? */
int8_t      param_rotation          = 0;        /*!< \brief  Enable rotation animation */

int8_t      param_helicoid          = 0;        /*!< \brief  Should target oscillate in helicoid shape? */
float       param_helicoid_r_max    = 0.5;      /*!< \brief  Helicoid X-Y circling radius */
float       param_helicoid_z_max    = 0.5;      /*!< \brief  Helicoid Z amplitude */
float       param_helicoid_r_freq   = 0.1;      /*!< \brief  Helicoid X-Y circling frequency */
float       param_helicoid_z_freq   = 0.1;      /*!< \brief  Helicoid Z oscillating frequency */

const v3D(float)  param_background_color  = {1.0, 1.0, 1.0};      /*!< \brief  Background color - constant */

/* ****************************************************************************
*           AUXILIARY GLOBAL VARIABLES (GetOpt)
******************************************************************************/

/*! \cond */

static const char * option_short = "h?";

static const struct option option_long[] = {
	{ "swarm-size",         required_argument,  NULL,   1   },

	{ "swarm-cognitive",    required_argument,  NULL,   2   },
	{ "swarm-social",       required_argument,  NULL,   3   },
	{ "swarm-inertia",      required_argument,  NULL,   4   },

	{ "swarm-vmax",         required_argument,  NULL,   5   },
	{ "swarm-dt",           required_argument,  NULL,   6   },

	{ "swarm-same_rand",    no_argument,        NULL,   7   },
	{ "swarm-rand_inertia", no_argument,        NULL,   8   },
	{ "swarm-known_best",   no_argument,        NULL,   9   },

	{ "anim-rotation",      no_argument,        NULL,  10   },
	{ "anim-force_FPS",     no_argument,        NULL,  11   },

	{ "helicoid-enable",    no_argument,        NULL,  12   },
	{ "helicoid-r_max",     required_argument,  NULL,  13   },
	{ "helicoid-z_max",     required_argument,  NULL,  14   },
	{ "helicoid-r_freq",    required_argument,  NULL,  15   },
	{ "helicoid-z_freq",    required_argument,  NULL,  16   },

	{ "help",               no_argument,        NULL,  'h'  },
	{ NULL,                 no_argument,        NULL,   0   }
};

/*! \endcond */

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Prints help and usage message to stdout
 */
void print_help() {
    printf(
        "PROGRAM\n"
        "    Particle Swarm 3D\n"
        "    version 1.0\n"
        "\n"
        "USAGE\n"
        "    ps3D [OPTION]\n"
        "\n"
        "DESCRIPTION\n"
        "    Visualizes Particle Swarm Algorithm in 3D.\n"
        "    The cost function that the algorithm is applied on is:\n"
        "		f(x,y) = x^2 + y^2 +z^2\n"
        "    The optimization space is in ranges:\n"
        "		x: [-1, +1]\n"
        "		y: [-1, +1]\n"
        "		z: [-1, +1]\n"
        "    in Cartesian coordinate system.\n"
        "\n"
        "    Options:\n"
        "       --swarm-size=<VAL>          Sets swarm size to VAL.\n"
        "       --swarm-cognitive=<VAL>     Sets cognitive coefficient to VAL.\n"
        "       --swarm-social=<VAL>        Sets social coefficient to VAL.\n"
        "       --swarm-inertia=<VAL>       Sets inertia coefficient to VAL.\n"
        "       --swarm-vmax=<VAL>          Sets maximum velocity to VAL.\n"
        "       --swarm-dt=<VAL>            Sets time step to VAL.\n"
        "\n"
        "       --swarm-same_rand=<VAL>     Cognitive, Social and Inertial\n"
        "                                   (optional) parts of formula are\n"
        "                                   multiplied by the same random number.\n"
        "       --swarm-rand_inertia=<VAL>  Inertial part of formula is also\n"
        "                                   multiplied by a random number.\n"
        "       --swarm-known_best=<VAL>    Sets current global best solution\n"
        "                                   of swarm to current function minimum.\n"
        "\n"
        "       --anim-rotation             Enable rotation animation\n"
        "       --anim-force_FPS            Force FPS to maximum\n"
        "\n"
        "       --helicoid-enable           Enables target moving around the\n"
        "                                   click point.\n"
        "       --helicoid-r_max=<VAL>      Helicoid's horizontal spinning radius\n"
        "       --helicoid-r_freq=<VAL>     Helicoid's horizontal spinning frequency\n"
        "       --helicoid-z_max=<VAL>      Helicoid's vertical oscillation amplitude\n"
        "       --helicoid-z_freq=<VAL>     Helicoid's vertical oscillation frequency\n"
        "\n"
        "       --help -h -?                Shows this message.\n"
        "\n"
        "LICENSE\n"
        "    Copyright 2015 Viktor Slavkovic <viktor.slavkovic@gmail.com>\n"
        "\n"
        "    This program is free software: you can redistribute it and/or modify\n"
        "    it under the terms of the GNU General Public License as published by\n"
        "    the Free Software Foundation, either version 3 of the License, or\n"
        "    (at your option) any later version.\n"
        "\n"
        "    This program is distributed in the hope that it will be useful,\n"
        "    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
        "    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
        "    GNU General Public License for more details.\n"
        "\n"
        "    You should have received a copy of the GNU General Public License\n"
        "    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n"
    );
}

/*!
 *  \brief Handles command-line arguments and sets parameters accordingly
 *  \param argc Number of command-line arguments
 *  \param argv Array of command-line arguments
 */
void handle_args(int argc, char *argv[]) {

    int option = 0;
	int long_idx = 0;

	while( (option = getopt_long( argc, argv, option_short, option_long, &long_idx )) != -1 ) {
		switch( option ) {
		    /* --swarm-size */
			case 1: {
                    uint64_t temp = strtoull(optarg, NULL, 10);
                    switch (temp) {
                        case 0ULL:
                        case ULLONG_MAX :   printf("Argument Error: Invalid value for swarm-size option.\n"); break;
                        default:            param_swarm_size = temp; break;
                    }
                }
                break;

			/* --swarm-cognitive */
			case 2: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-cognitive option.\n");
                    else param_swarm_cognitive = temp;
                }
                break;

			/* --swarm-social */
			case 3: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-social option.\n");
                    else param_swarm_social = temp;
                }
                break;

			/* --swarm-inertia */
			case 4: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-inertia option.\n");
                    else param_swarm_inertia = temp;
                }
                break;

            /* --swarm-vmax */
			case 5: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-vmax option.\n");
                    else param_swarm_vmax = temp;
                }
                break;

            /* --swarm-dt */
			case 6: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-dt option.\n");
                    else param_swarm_dt = temp;
                }
                break;

            /* --swarm-same_rand */
			case 7:
			    param_swarm_same_rand = 1;
                break;

            /* --swarm-rand_inertia */
			case 8:
			    param_swarm_randw = 1;
                break;

            /* --swarm-known_best */
			case 9:
			    param_swarm_kb = 1;
                break;

            /* --anim-rotation */
			case 10:
			    param_rotation = 1;
                break;

            /* --anim-force_FPS */
			case 11:
			    param_force_FPS = 1;
                break;

            /* --helicoid-enable */
			case 12:
			    param_helicoid = 1;
                break;

            /* --helicoid-r_max */
			case 13: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-r_max option.\n");
                    else if (!param_helicoid)
                        printf("Argument Error: Helicoid has to be enabled before setting it's options.\n");
                    else param_helicoid_r_max = temp;
                }
                break;

            /* --helicoid-z_max */
			case 14: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-z_max option.\n");
                    else if (!param_helicoid)
                        printf("Argument Error: Helicoid has to be enabled before setting it's options.\n");
                    else param_helicoid_z_max = temp;
                }
                break;

            /* --helicoid-r_freq */
			case 15: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-r_freq option.\n");
                    else if (!param_helicoid)
                        printf("Argument Error: Helicoid has to be enabled before setting it's options.\n");
                    else param_helicoid_r_freq = temp;
                }
                break;

            /* --helicoid-z_freq */
			case 16: {
                    float temp = strtof(optarg, NULL);
                    if  (temp == 0.0F || temp == +HUGE_VALF || temp == -HUGE_VALF)
                        printf("Argument Error: Invalid value for swarm-z_freq option.\n");
                    else if (!param_helicoid)
                        printf("Argument Error: Helicoid has to be enabled before setting it's options.\n");
                    else param_helicoid_z_freq = temp;
                }
                break;

            /* --help, -h, -? */
			case 'h':
			case '?':
				print_help();
				exit(0);
				break;

			default: break;
		}
	}
}

#endif // PARAMETERS_H_INCLUDED

