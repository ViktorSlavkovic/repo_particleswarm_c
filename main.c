/*!
 * \file    main.c
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Main source file
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
 *
*/

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdint.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

#include "transform/transform.h"
#include "graphics.h"
#include "swarm/swarm.h"
#include "objects/o_swarm.h"
#include "objects/o_axes.h"
#include "parameters.h"

/* ****************************************************************************
*           GLOBAL VARIABLES
******************************************************************************/


swarm3D *S;                             /*!< \brief Pointer to swarm3D */

v3D(float) f_min = {0.0f, 0.0f, 0.0f};  /*!< \brief Vector at which f_cost has minimum */

graphics G;                             /*!< \brief Graphics machinery */

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief The cost function which the algorithm is running on
 *  \param v 3D Vector
 *  \param t Time
 *  \return The calculated value of cost function
 */
float f_cost(const v3D(float) v, float t) {

    static float own_t=-1.0f;
    if (own_t != t) {
        own_t = t;
        if (param_helicoid) {
            f_min.z = param_helicoid_z_max*sinf(t*param_helicoid_z_freq);
            f_min.x = param_helicoid_r_max*cosf(t*param_helicoid_r_freq);
            f_min.y = param_helicoid_r_max*sinf(t*param_helicoid_r_freq);
        }
    }

    return      (v.x-f_min.x)*(v.x-f_min.x)
            +   (v.y-f_min.y)*(v.y-f_min.y)
            +   (v.z-f_min.z)*(v.z-f_min.z);
}

/*!
 *  \brief Initializes camera and perspective projection
 */
void initViewOptions() {
    setCameraTranslation((v3D(float)){5.0f, 5.0f, 2.5f}, &G.camera);
    setCameraRotation((v3D(float)){0,0,1}, (v3D(float)){-1, -1, -0.5}, &G.camera);
    G.perspective.ar = (float)G.w / G.h;
    G.perspective.fov = 3.14 / 6;
    G.perspective.z_near = 0.0;
    G.perspective.z_far = 100.0;
}

/*!
 *  \brief Main function
 *  \param argc Number of command-line arguments
 *  \param argv Array of command-line arguments
 */
int main(int argc, char ** argv) {

    handle_args(argc, argv);

    graphics_Init(&G, param_force_FPS, param_background_color);
    graphics_InitShader(&G);
    graphics_CenterMouse(&G);

    initViewOptions();

    S = swarm3D_create  (   f_cost,
                            param_swarm_size,
                            param_swarm_vmax,
                            param_swarm_min_bound,
                            param_swarm_max_bound
                        );

    S->c_cognitive = param_swarm_cognitive;
    S->c_social = param_swarm_social;
    S->c_w = param_swarm_inertia;

    o_swarm_Create(S);
    o_axes_Create();

    float move_step = 0.07;
    float angle_step = -3.14 / 256;
    float angle = 0;

    int8_t running = 1;
    while (running) {

        swarm3D_update  (   S,
                            param_swarm_dt,
                            param_swarm_randw,
                            param_swarm_same_rand
                        );

        if (param_swarm_kb) S->gb = f_min;

        /* TRANSFORMATION MATRIX */

        m4D(float) T;
        getCameraTransform(&(G.camera), &T);
        concatLeftPerspectiveProjection(&(G.perspective), &T);
        if (param_rotation)
            concatRightRotation((v3D(float)){0.0,0.0,angle-=angle_step},&T);
        graphics_PushMatrix(&G, (const m4D(float)*) &T);

        /* DRAW */
        o_swarm_Draw(G.shader_RGBA, S, f_min);
        o_axes_Draw(G.shader_RGBA);
        graphics_Refresh(&G);

        /* HANDLE EVENTS */
        static SDL_Event event;
        while (SDL_PollEvent(&event)) switch (event.type) {

                /* QUIT on window close */
                case SDL_QUIT:
                    running = 0;
                    break;

                /* ROTATE CAMERA on mouse movement */
                case SDL_MOUSEMOTION: {

                    int cx = event.motion.x;
                    int cy = event.motion.y;

                    if (cy<G.h/2) rotateUpCamera((float)(G.h/2-cy)/1000.0f, &G.camera);
                    else rotateDownCamera((float)(cy-G.h/2)/1000.0f, &G.camera);

                    if (cx<G.w/2) rotateLeftCamera((float)(G.w/2-cx)/1000.0f, &G.camera);
                    else rotateRightCamera((float)(cx-G.w/2)/1000.0f, &G.camera);

                    graphics_CenterMouse(&G);
                }
                break;

                /*    MOVE on keyboard W,A,S,D; QUIT on keyboard Esc */
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {

                        case SDLK_ESCAPE:
                            running = 0;
                            break;

                        case SDLK_w: {
                            v3D(float) v = v3D_mulc(float)(G.camera.target, move_step);
                            moveCamera(v, &G.camera);
                            break;
                        }

                        case SDLK_s: {
                            v3D(float) v = v3D_mulc(float)(G.camera.target, -move_step);
                            moveCamera(v, &G.camera);
                            break;
                        }

                        case SDLK_d: {
                            v3D(float) right = v3D_mulCross(float)(G.camera.target, G.camera.up);
                            v3D(float) v = v3D_mulc(float)(right, move_step);
                            moveCamera(v, &G.camera);
                            break;
                        }

                        case SDLK_a: {
                            v3D(float) left = v3D_mulCross(float)(G.camera.up, G.camera.target);
                            v3D(float) v = v3D_mulc(float)(left, move_step);
                            moveCamera(v, &G.camera);
                            break;
                        }
                    }
                    break;

            } /* event handle loop */

    } /* main loop */

    swarm3D_destroy(S);
    graphics_UnInit(&G);

    return 0;
}
