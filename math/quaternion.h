/*!
 * \file    quaternion.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of quasi-template type of quaternion and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
 */

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <math.h>
#include <stdint.h>
#include <stdarg.h>

/* ****************************************************************************
*           MACROS
******************************************************************************/

#ifndef Q

    #define _pom_Q(type) Q_##type
    #define Q(type) _pom_Q(type)

    #define _pom_Q_add(type) Q_add_##type
    #define Q_add(type) _pom_Q_add(type)

    #define _pom_Q_sub(type) Q_sub_##type
    #define Q_sub(type) _pom_Q_sub(type)

    #define _pom_Q_mulHamilton(type) Q_mulHamilton_##type
    #define Q_mulHamilton(type) _pom_Q_mulHamilton(type)

    #define _pom_Q_conjugate(type) Q_conjugate_##type
    #define Q_conjugate(type) _pom_Q_conjugate(type)

    #define _pom_Q_norm(type) Q_norm_##type
    #define Q_norm(type) _pom_Q_norm(type)

#endif // Q

/* ****************************************************************************
*           QUATERNION STRUCTURE
******************************************************************************/

/*!
 * \struct Q
 * \brief Quasi-template quaternion. Call it with: Q(TYPE)
 */
typedef struct {
    q_type Re;      /**< Real part */
    v3D(q_type) Im; /**< Pure imaginary part as 3D vector */
} Q(q_type);

/* ***********************************************************************************************************************
*           FUNCTIONS
*************************************************************************************************************************/

Q(q_type) Q_add(q_type)         (const Q(q_type) q1, const Q(q_type) q2) {
    Q(q_type) pom;
    pom.Re = q1.Re + q2.Re;
    pom.Im = v3D_add(q_type)(q1.Im, q2.Im);
    return pom;
}

Q(q_type) Q_sub(q_type)         (const Q(q_type) q1, const Q(q_type) q2) {
    Q(q_type) pom;
    pom.Re = q1.Re - q2.Re;
    pom.Im = v3D_sub(q_type)(q1.Im, q2.Im);
    return pom;
}

Q(q_type) Q_mulHamilton(q_type) (const Q(q_type) q1, const Q(q_type) q2) {
    Q(q_type) pom;
    pom.Re   =   q1.Re*q2.Re - q1.Im.x*q2.Im.x - q1.Im.y*q2.Im.y - q1.Im.z*q2.Im.z;
    pom.Im.x = q1.Re*q2.Im.x +   q1.Im.x*q2.Re + q1.Im.y*q2.Im.z - q1.Im.z*q2.Im.y;
    pom.Im.y = q1.Re*q2.Im.y - q1.Im.x*q2.Im.z +   q1.Im.y*q2.Re + q1.Im.z*q2.Im.x;
    pom.Im.z = q1.Re*q2.Im.z + q1.Im.x*q2.Im.y - q1.Im.y*q2.Im.x +   q1.Im.z*q2.Re;
    return pom;
}

Q(q_type) Q_conjugate(q_type)   (const Q(q_type) q) {
    Q(q_type) pom;
    pom.Re = q.Re;
    pom.Im = v3D_mulc(q_type)(q.Im,-1);
    return pom;
}

double  Q_norm(q_type)          (const Q(q_type) q) {
    double a = (double) q.Re;
    double b = (double) q.Im.x;
    double c = (double) q.Im.y;
    double d = (double) q.Im.z;
    return sqrt(a*a+b*b+c*c+d*d);
}
