/*!
 * \file    vector4D.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of quasi-template structure of 4D vector and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
 *
 * Ovaj fajl je namenjen iskljucivo za kvazigenericko
 * kreiranje tipova vektora. Upotreba za pravljenje tipa
 * vektora odredjenog tipa je sledeca:
 *
 *          #define v4D_type TIP
 *          #include "vector4D.h"
 *          #undef v4D_type
 *
 * Tip koji se koristi mora imati definisane operatore:
 *
 *          +, -, *, /, =
 *
 * Ako je pri kreiranju tipa definisan makro
 *
 *          #define v4D_custom_allignment BROJ
 *
 * svi elementi strukture se allign-uju na BROJ, pa
 * to resava problem cast-a pokazivaca na vektore
 * razlicitog tipa, ali se mogu javiti problemi ako
 * se cast vrsi iz manjeg u veci tip i naravno, nije
 * moguce vrsiti cast int <-> float i slicno. Ovo radi
 * na GNU (GCC) i Microsoft (MSVC) C komajlerima
*/

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <math.h>
#include <stdint.h>
#include <stdarg.h>

/* ****************************************************************************
*           MACROS
******************************************************************************/

#ifndef v4D

    /* Pomocni makro resava problem razresavanja makroa kada se koristi ## */
    #define _pom_v4D(type) v4D_##type
    #define v4D(type) _pom_v4D(type)

    /* Makro za template konverziju vecktora. Poziva se sa: v4D_cast(novi_tip, vektor) */
    #define v4D_cast(type_to, A) (v4D(type_to)){(type_to)A.x,(type_to)A.y,(type_to)A.z, (type_to)A.w}

    /* Sledeci makroi sluze za pozivanje funkcija navedenih ispod. Poziv je opisan u komentaru svake funckije. */

    #define _pom_v4D_add(type) v4D_add_##type
    #define v4D_add(type) _pom_v4D_add(type)

    /* EXPERIMENTAL */
    #define _pom_v4D_addn(type) v4D_addn_##type
    #define v4D_addn(type) _pom_v4D_addn(type)

    #define _pom_v4D_sub(type) v4D_sub_##type
    #define v4D_sub(type) _pom_v4D_sub(type)

    #define _pom_v4D_mul(type) v4D_mul_##type
    #define v4D_mul(type) _pom_v4D_mul(type)

    /* EXPERIMENTAL */
    #define _pom_v4D_muln(type) v4D_muln_##type
    #define v4D_muln(type) _pom_v4D_muln(type)

    #define _pom_v4D_div(type) v4D_div_##type
    #define v4D_div(type) _pom_v4D_div(type)

    #define _pom_v4D_addc(type) v4D_addc_##type
    #define v4D_addc(type) _pom_v4D_addc(type)

    #define _pom_v4D_subc(type) v4D_subc_##type
    #define v4D_subc(type) _pom_v4D_subc(type)

    #define _pom_v4D_mulc(type) v4D_mulc_##type
    #define v4D_mulc(type) _pom_v4D_mulc(type)

    #define _pom_v4D_divc(type) v4D_divc_##type
    #define v4D_divc(type) _pom_v4D_divc(type)

    #define _pom_v4D_modulus(type) v4D_modulus_##type
    #define v4D_modulus(type) _pom_v4D_modulus(type)

    /* NEW Function*/
    #define _pom_v4D_sum(type) v4D_sum_##type
    #define v4D_sum(type) _pom_v4D_sum(type)

#endif // v4D

/* ****************************************************************************
*           4D VECTOR STRUCTURE
******************************************************************************/

/*!
 * \struct v4D
 * \brief Quasi-template 4D vector. Call it with: v4D(TYPE)
 */

#ifdef v4D_custom_allignment

    #if defined(__GNUC__)

        typedef struct {
            v4D_type x __attribute__ ((aligned (v4D_custom_allignment))); /**< x coordinate */
            v4D_type y __attribute__ ((aligned (v4D_custom_allignment))); /**< y coordinate */
            v4D_type z __attribute__ ((aligned (v4D_custom_allignment))); /**< z coordinate */
            v4D_type w __attribute__ ((aligned (v4D_custom_allignment))); /**< w coordinate */
        } v4D(v4D_type);

    #elif defined(_MSC_VER)

        typedef struct {
            __declspec(align(v4D_custom_allignment)) v4D_type x;  /**< x coordinate */
            __declspec(align(v4D_custom_allignment)) v4D_type y;  /**< y coordinate */
            __declspec(align(v4D_custom_allignment)) v4D_type z;  /**< z coordinate */
            __declspec(align(v4D_custom_allignment)) v4D_type w;  /**< w coordinate */
        } v4D(v4D_type);

    #else

        #error Do not know how to setup allignment with your compiler

    #endif // complier checking

#else

    typedef struct {
        v4D_type x; /**< x coordinate */
        v4D_type y; /**< y coordinate */
        v4D_type z; /**< z coordinate */
        v4D_type w; /**< z coordinate */
        /*
            mogli bi ovde da se stave pokazivaci na svaku od tih funckija,
            ali prvo sto bi zauzimali dodatnu memoriju, drugo sto bi
            trebalo dodeliti im pokazivace na ove implementirane funckije
            prilikom inicijalizacije, a opet ne bi bilo toliko lepo jer ispada
            da imamo v1 = v1.add(v1,v2) sto je mnogo ponavaljanja v1 za v1+=v2
            ili v1 = v1+v2
        */
    } v4D(v4D_type);

#endif

/* ***********************************************************************************************************************
*           FUNCTIONS
*************************************************************************************************************************/

/* Funckija za sabiranje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v4D_add(tip)(a,b); */
v4D(v4D_type) v4D_add(v4D_type)     (const v4D(v4D_type) v1, const v4D(v4D_type) v2) {
    v4D(v4D_type) pom;
    pom.x = v1.x + v2.x;
    pom.y = v1.y + v2.y;
    pom.z = v1.z + v2.z;
    pom.w = v1.w + v2.w;
    return pom;
}

/* Funckija za sabiranje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v4D_addn(tip)(n,a1,a2,a3,..,an); */
v4D(v4D_type) v4D_addn(v4D_type)     (uint8_t n, ... ) {

    va_list arguments;
    va_start (arguments, n);

    v4D(v4D_type) pom = {0,0,0};

    while(n--)
        pom = v4D_add(v4D_type)(pom, va_arg ( arguments, v4D(v4D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za oduzimanje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v4D_sub(tip)(a,b); */
v4D(v4D_type) v4D_sub(v4D_type)     (const v4D(v4D_type) v1, const v4D(v4D_type) v2) {
    v4D(v4D_type) pom;
    pom.x = v1.x - v2.x;
    pom.y = v1.y - v2.y;
    pom.z = v1.z - v2.z;
    pom.w = v1.w - v2.w;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v4D_mul(tip)(a,b); */
v4D(v4D_type) v4D_mul(v4D_type)     (const v4D(v4D_type) v1, const v4D(v4D_type) v2) {
    v4D(v4D_type) pom;
    pom.x = v1.x * v2.x;
    pom.y = v1.y * v2.y;
    pom.z = v1.z * v2.z;
    pom.w = v1.w * v2.w;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v4D_muln(tip)(n,a1,a2,a3,..,an); */
v4D(v4D_type) v4D_muln(v4D_type)     (uint8_t n, v4D(v4D_type) v1, ... ) {
    va_list arguments;
    va_start (arguments, v1);

    v4D(v4D_type) pom = v1; n--;

    while(n--)
        pom = v4D_mul(v4D_type)(pom, va_arg ( arguments, v4D(v4D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za deljenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v4D_div(tip)(a,b); */
v4D(v4D_type) v4D_div(v4D_type)     (const v4D(v4D_type) v1, const v4D(v4D_type) v2) {
    v4D(v4D_type) pom;
    pom.x = v1.x / v2.x;
    pom.y = v1.y / v2.y;
    pom.z = v1.z / v2.z;
    pom.w = v1.w / v2.w;
    return pom;
}

/* Funckija za sabiranje vektora sa konstantom istog tipa. Poziva se sa: v4D_addc(tip)(a,c); */
v4D(v4D_type) v4D_addc(v4D_type)    (const v4D(v4D_type) v, const v4D_type c) {
    v4D(v4D_type) pom;
    pom.x = v.x + c;
    pom.y = v.y + c;
    pom.z = v.z + c;
    pom.w = v.w + c;
    return pom;
}

/* Funckija za oduzimanje konstante od vektora istog tipa. Poziva se sa: v4D_subc(tip)(a,c); */
v4D(v4D_type) v4D_subc(v4D_type)    (const v4D(v4D_type) v, const v4D_type c) {
    v4D(v4D_type) pom;
    pom.x = v.x - c;
    pom.y = v.y - c;
    pom.z = v.z - c;
    pom.w = v.w - c;
    return pom;
}

/* Funckija za mnozenje vektora konstantom istog tipa. Poziva se sa: v4D_mulc(tip)(a,c); */
v4D(v4D_type) v4D_mulc(v4D_type)    (const v4D(v4D_type) v, const v4D_type c) {
    v4D(v4D_type) pom;
    pom.x = v.x * c;
    pom.y = v.y * c;
    pom.z = v.z * c;
    pom.w = v.w * c;
    return pom;
}

/* Funckija za deljenje vektora konstantom istog tipa. Poziva se sa: v4D_divc(tip)(a,c); */
v4D(v4D_type) v4D_divc(v4D_type)    (const v4D(v4D_type) v, const v4D_type c) {
    v4D(v4D_type) pom;
    pom.x = v.x / c;
    pom.y = v.y / c;
    pom.z = v.z / c;
    pom.w = v.w / c;
    return pom;
}

/* Funckija za modul vektora */
double v4D_modulus(v4D_type)  (const v4D(v4D_type) v) {
    double x = (double) v.x;
    double y = (double) v.y;
    double z = (double) v.z;
    double w = (double) v.w;
    return sqrt(x*x+y*y+z*z+w*w);
}

/* Funckija za sumu koordinata vektora */
double v4D_sum(v4D_type)  (const v4D(v4D_type) v) {
   return v.x+v.y+v.z+v.w;
}


