/*!
 * \file    math_includer.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Includes quasi-template vectors, matrices and quaternion. Needs macro math_type do be defined.
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#define v2D_type math_type
#include "vector2D.h"
#undef v2D_type

#define v3D_type math_type
#include "vector3D.h"
#undef v3D_type

#define v4D_type math_type
#include "vector4D.h"
#undef v4D_type

#define m4D_type math_type
#include "matrix4D.h"
#undef m4D_type

#define q_type math_type
#include "quaternion.h"
#undef m4D_type
