/*!
 * \file    matrix4D.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of quasi-template type of 4x4 matrix and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
 */

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <memory.h>
#include <assert.h>

/* ****************************************************************************
*           MACROS
******************************************************************************/

#ifndef m4D

    #define _pom_m4D(type) m4D_##type
    #define m4D(type) _pom_m4D(type)

    #define m4D_cast(type_to, A) (m4D(type_to)){(type_to)A.x,(type_to)A.y}

    #define _pom_m4D_identity(type) m4D_identity_##type
    #define m4D_identity(type) _pom_m4D_identity(type)

    #define _pom_m4D_zeros(type) m4D_zeros_##type
    #define m4D_zeros(type) _pom_m4D_zeros(type)

    #define _pom_m4D_ones(type) m4D_ones_##type
    #define m4D_ones(type) _pom_m4D_ones(type)

    #define _pom_m4D_transpose(type) m4D_transpose_##type
    #define m4D_transpose(type) _pom_m4D_transpose(type)

    #define _pom_m4D_add(type) m4D_add_##type
    #define m4D_add(type) _pom_m4D_add(type)

    #define _pom_m4D_mulDot(type) m4D_mulDot_##type
    #define m4D_mulDot(type) _pom_m4D_mulDot(type)

    #define _pom_m4D_mulCross(type) m4D_mulCross_##type
    #define m4D_mulCross(type) _pom_m4D_mulCross(type)

    /* EXPERIMENTAL */
    #define _pom_m4D_mulCrossN(type) m4D_mulCrossN_##type
    #define m4D_mulCrossN(type) _pom_m4D_mulCrossN(type)

#endif // m4D

/* ****************************************************************************
*           4x4 MATRIX TYPE
******************************************************************************/

/*! \cond */
typedef v4D(m4D_type) m4D(m4D_type)[4];
/*! \endcond */

/* ***********************************************************************************************************************
*           FUNCTIONS
*************************************************************************************************************************/

void m4D_identity(m4D_type)         (m4D(m4D_type) * result) {
    (*result)[0] = (v4D(m4D_type)){1, 0, 0, 0};
    (*result)[1] = (v4D(m4D_type)){0, 1, 0, 0};
    (*result)[2] = (v4D(m4D_type)){0, 0, 1, 0};
    (*result)[3] = (v4D(m4D_type)){0, 0, 0, 1};
}

void m4D_zeros(m4D_type)            (m4D(m4D_type) * result) {
    memset((*result),0,sizeof(m4D(m4D_type)));
}

void m4D_ones(m4D_type)             (m4D(m4D_type) * result) {
    int i;
    for (i=0; i<4; i++) (*result)[i] = (v4D(m4D_type)){1, 1, 1, 1};
}

void m4D_transpose(m4D_type)        (const m4D(m4D_type) m, m4D(m4D_type) * result) {

    m4D(m4D_type) copy_m;
    memcpy((void*)copy_m, (const void*)m, sizeof(m4D(m4D_type)));

    (*result)[0] = (v4D(m4D_type)){m[0].x, m[1].x, m[2].x, m[3].x};
    (*result)[1] = (v4D(m4D_type)){m[0].y, m[1].y, m[2].y, m[3].y};
    (*result)[2] = (v4D(m4D_type)){m[0].z, m[1].z, m[2].z, m[3].z};
    (*result)[3] = (v4D(m4D_type)){m[0].w, m[1].w, m[2].w, m[3].w};
}

void m4D_add(m4D_type)              (const m4D(m4D_type) m1, const m4D(m4D_type) m2, m4D(m4D_type) * result) {
    m4D(m4D_type) copy_m1;
    memcpy((void*)copy_m1, (const void*)m1, sizeof(m4D(m4D_type)));
    m4D(m4D_type) copy_m2;
    memcpy((void*)copy_m2, (const void*)m2, sizeof(m4D(m4D_type)));

    int i;
    for (i=0; i<4; i++)
        (*result)[i] = v4D_add(m4D_type) (copy_m1[i],copy_m2[i]);
}

void m4D_mulDot(m4D_type)           (const m4D(m4D_type) m1, const m4D(m4D_type) m2, m4D(m4D_type) * result) {

    m4D(m4D_type) copy_m1;
    memcpy((void*)copy_m1, (const void*)m1, sizeof(m4D(m4D_type)));
    m4D(m4D_type) copy_m2;
    memcpy((void*)copy_m2, (const void*)m2, sizeof(m4D(m4D_type)));

    int i;
    for (i=0; i<4; i++) {
        (*result)[i] = v4D_mul(m4D_type)(copy_m1[i],copy_m2[i]);
    }
}

void m4D_mulCross(m4D_type)         (const m4D(m4D_type) m1, const m4D(m4D_type) m2, m4D(m4D_type) * result) {

    m4D(m4D_type) copy_m1;
    memcpy((void*)copy_m1, (const void*)m1, sizeof(m4D(m4D_type)));

    m4D(m4D_type) m2t;
    m4D_transpose(m4D_type)(m2,&m2t);
    int i;
    for (i=0; i<4; i++) {
        (*result)[i].x = v4D_sum(m4D_type) ( v4D_mul(m4D_type)(copy_m1[i],m2t[0]) );
        (*result)[i].y = v4D_sum(m4D_type) ( v4D_mul(m4D_type)(copy_m1[i],m2t[1]) );
        (*result)[i].z = v4D_sum(m4D_type) ( v4D_mul(m4D_type)(copy_m1[i],m2t[2]) );
        (*result)[i].w = v4D_sum(m4D_type) ( v4D_mul(m4D_type)(copy_m1[i],m2t[3]) );
    }
}

void m4D_mulCrossN(m4D_type)        (m4D(m4D_type) * result, uint8_t n, ... ) {
    m4D_identity(m4D_type)(result);

    va_list arguments;
    va_start (arguments, n);

    while(n--)
        m4D_mulCross(m4D_type)(*result, * va_arg ( arguments, m4D(m4D_type)* ), result );

    va_end (arguments);
}
