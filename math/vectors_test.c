/*!
* \file    main.c
*
* \author  Viktor Slavkovic
*
* Ovo je program koji testira kvazigenericke vektore
* iz zaglavlja vector2D.h i vector3D.h
*/

#include <stdio.h>
#include <memory.h>

/*
    ovaj makro omogucava da se postavi allignment
    clanova strukture na istu vrednost i time
    omoguci cast pokazivaca na vektore razlicitog tipa,
    a ako se ne definise, allignment je podrazumevani.
    Ovo treba koristiti pazljivo (opis u header fajlu)
 */

#define vector2D_custom_allignment 4

/* ukljucujem vector2D(int) */
#define vector2D_type int
#include "vector2D.h"
#undef vector2D_type

/* ukljucujem vector2D(char) */
#define vector2D_type char
#include "vector2D.h"
#undef vector2D_type

/* ukljucujem vector2D(double) */
#define vector2D_type double
#include "vector2D.h"
#undef vector2D_type

#undef vector2D_custom_allignment

void test_2D() {

    printf("\n------- Test 2D -------\n");

    vector2D(int) a = {1,2};
    vector2D(int) b = {3,4};

    vector2D(int) c = vector2D_add(int)(a,b);
    printf("%d %d\n",c.x,c.y);

    vector2D(double) d = vector2D_cast(double, c);
    printf("%f %f\n",d.x,d.y);

    vector2D(double) e = vector2D_divc(double)(vector2D_cast(double, c), 2.0);
    printf("%f %f\n",e.x,e.y);

    /*
        ALLIGNMENT test
    */

    printf("Sizeof vectr2D(int): %d\n",sizeof(vector2D(int)));
    printf("Sizeof vectr2D(char): %d\n",sizeof(vector2D(char)));

    vector2D(char) * p = (vector2D(char)*) &c;
    printf("%d %d\n",p->x,p->y);

    vector2D(char) f = {12,17};
    /*
       supline allignement-a nisu automatski postavljene na 0, pa je ovo problem sa cast-om;
       zato treba pazljivo koristiti ove cast-ove, posebno kad se cast-uje iz manjeg u veci tip;
	   ovde program puca ako se iskljuci vector2D_custom_allignment
    */
    memset(((char*)&f)+1,0,3); /* memset(((void*)&f)+sizeof(char),0,sizeof(f)/2-sizeof(char)) */
    memset(((char*)&f)+5,0,3); /* memset(((void*)&f)+sizeof(char)+sizeof(f)/2,0,sizeof(f)/2-sizeof(char)) */

    vector2D(int) * p1 = (vector2D(int)*) &f;
    printf("%d %d\n",p1->x,p1->y);
}

/* ukljucujem vector3D(int) */
#define vector3D_type int
#include "vector3D.h"
#undef vector3D_type

/* ukljucujem vector3D(double) */
#define vector3D_type double
#include "vector3D.h"
#undef vector3D_type

void test_3D() {

    printf("\n------- Test 3D -------\n");

    vector3D(int) a = {1,2,3};
    vector3D(int) b = {4,5,6};

    vector3D(int) c = vector3D_add(int)(a,b);
    printf("%d %d %d\n",c.x,c.y,c.z);

    vector3D(double) d = vector3D_cast(double, c);
    printf("%f %f %f\n",d.x,d.y,d.z);

    vector3D(double) e = vector3D_divc(double)(vector3D_cast(double, c), 2.0);
    printf("%f %f %f\n",e.x,e.y,e.z);

}

int main() {

    test_2D();
    test_3D();

    return 0;
}
