/*!
 * \file    vector3D.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of quasi-template structure of 3D vector and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
 *
 * Ovaj fajl je namenjen iskljucivo za kvazigenericko
 * kreiranje tipova vektora. Upotreba za pravljenje tipa
 * vektora odredjenog tipa je sledeca:
 *
 *          #define v3D_type TIP
 *          #include "vector3D.h"
 *          #undef v3D_type
 *
 * Tip koji se koristi mora imati definisane operatore:
 *
 *          +, -, *, /, =
 *
 * Ako je pri kreiranju tipa definisan makro
 *
 *          #define v3D_custom_allignment BROJ
 *
 * svi elementi strukture se allign-uju na BROJ, pa
 * to resava problem cast-a pokazivaca na vektore
 * razlicitog tipa, ali se mogu javiti problemi ako
 * se cast vrsi iz manjeg u veci tip i naravno, nije
 * moguce vrsiti cast int <-> float i slicno. Ovo radi
 * na GNU (GCC) i Microsoft (MSVC) C komajlerima
*/

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <math.h>
#include <stdint.h>
#include <stdarg.h>

/* ****************************************************************************
*           MACROS
******************************************************************************/

#ifndef v3D

    /* Pomocni makro resava problem razresavanja makroa kada se koristi ## */
    #define _pom_v3D(type) v3D_##type
    #define v3D(type) _pom_v3D(type)

    /* Makro za template konverziju vecktora. Poziva se sa: v3D_cast(novi_tip, vektor) */
    #define v3D_cast(type_to, A) (v3D(type_to)){(type_to)A.x,(type_to)A.y,(type_to)A.z}

    /* Sledeci makroi sluze za pozivanje funkcija navedenih ispod. Poziv je opisan u komentaru svake funckije. */

    #define _pom_v3D_add(type) v3D_add_##type
    #define v3D_add(type) _pom_v3D_add(type)

    /* EXPERIMENTAL */
    #define _pom_v3D_addn(type) v3D_addn_##type
    #define v3D_addn(type) _pom_v3D_addn(type)

    #define _pom_v3D_sub(type) v3D_sub_##type
    #define v3D_sub(type) _pom_v3D_sub(type)

    #define _pom_v3D_mul(type) v3D_mul_##type
    #define v3D_mul(type) _pom_v3D_mul(type)

    /* EXPERIMENTAL */
    #define _pom_v3D_muln(type) v3D_muln_##type
    #define v3D_muln(type) _pom_v3D_muln(type)

    /* NEW Function*/
    #define _pom_v3D_mulCross(type) v3D_mulCross_##type
    #define v3D_mulCross(type) _pom_v3D_mulCross(type)

    #define _pom_v3D_div(type) v3D_div_##type
    #define v3D_div(type) _pom_v3D_div(type)

    #define _pom_v3D_addc(type) v3D_addc_##type
    #define v3D_addc(type) _pom_v3D_addc(type)

    #define _pom_v3D_subc(type) v3D_subc_##type
    #define v3D_subc(type) _pom_v3D_subc(type)

    #define _pom_v3D_mulc(type) v3D_mulc_##type
    #define v3D_mulc(type) _pom_v3D_mulc(type)

    #define _pom_v3D_divc(type) v3D_divc_##type
    #define v3D_divc(type) _pom_v3D_divc(type)

    #define _pom_v3D_modulus(type) v3D_modulus_##type
    #define v3D_modulus(type) _pom_v3D_modulus(type)

    /* NEW Function*/
    #define _pom_v3D_normalize(type) v3D_normalize_##type
    #define v3D_normalize(type) _pom_v3D_normalize(type)

#endif // v3D

/* ****************************************************************************
*           3D VECTOR STRUCTURE
******************************************************************************/

/*!
 * \struct v3D
 * \brief Quasi-template 3D vector. Call it with: v3D(TYPE)
 */

#ifdef v3D_custom_allignment

    #if defined(__GNUC__)

        typedef struct {
            v3D_type x __attribute__ ((aligned (v3D_custom_allignment))); /**< x coordinate */
            v3D_type y __attribute__ ((aligned (v3D_custom_allignment))); /**< y coordinate */
            v3D_type z __attribute__ ((aligned (v3D_custom_allignment))); /**< z coordinate */
        } v3D(v3D_type);

    #elif defined(_MSC_VER)

        typedef struct {
            __declspec(align(v3D_custom_allignment)) v3D_type x;  /**< x coordinate */
            __declspec(align(v3D_custom_allignment)) v3D_type y;  /**< y coordinate */
            __declspec(align(v3D_custom_allignment)) v3D_type z;  /**< z coordinate */
        } v3D(v3D_type);

    #else

        #error Do not know how to setup allignment with your compiler

    #endif // complier checking

#else

    typedef struct {
        v3D_type x; /**< x coordinate */
        v3D_type y; /**< y coordinate */
        v3D_type z; /**< z coordinate */
        /*
            mogli bi ovde da se stave pokazivaci na svaku od tih funckija,
            ali prvo sto bi zauzimali dodatnu memoriju, drugo sto bi
            trebalo dodeliti im pokazivace na ove implementirane funckije
            prilikom inicijalizacije, a opet ne bi bilo toliko lepo jer ispada
            da imamo v1 = v1.add(v1,v2) sto je mnogo ponavaljanja v1 za v1+=v2
            ili v1 = v1+v2
        */
    } v3D(v3D_type);

#endif

/* ***********************************************************************************************************************
*           FUNCTIONS
*************************************************************************************************************************/

/* Funckija za sabiranje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v3D_add(tip)(a,b); */
v3D(v3D_type) v3D_add(v3D_type)     (const v3D(v3D_type) v1, const v3D(v3D_type) v2) {
    v3D(v3D_type) pom;
    pom.x = v1.x + v2.x;
    pom.y = v1.y + v2.y;
    pom.z = v1.z + v2.z;
    return pom;
}

/* Funckija za sabiranje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v3D_addn(tip)(n,a1,a2,a3,..,an); */
v3D(v3D_type) v3D_addn(v3D_type)     (uint8_t n, ... ) {

    va_list arguments;
    va_start (arguments, n);

    v3D(v3D_type) pom = {0,0,0};

    while(n--)
        pom = v3D_add(v3D_type)(pom, va_arg ( arguments, v3D(v3D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za oduzimanje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v3D_sub(tip)(a,b); */
v3D(v3D_type) v3D_sub(v3D_type)     (const v3D(v3D_type) v1, const v3D(v3D_type) v2) {
    v3D(v3D_type) pom;
    pom.x = v1.x - v2.x;
    pom.y = v1.y - v2.y;
    pom.z = v1.z - v2.z;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v3D_mul(tip)(a,b); */
v3D(v3D_type) v3D_mul(v3D_type)     (const v3D(v3D_type) v1, const v3D(v3D_type) v2) {
    v3D(v3D_type) pom;
    pom.x = v1.x * v2.x;
    pom.y = v1.y * v2.y;
    pom.z = v1.z * v2.z;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v3D_muln(tip)(n,a1,a2,a3,..,an); */
v3D(v3D_type) v3D_muln(v3D_type)     (uint8_t n, const v3D(v3D_type) v1, ... ) {
    va_list arguments;
    va_start (arguments, v1);

    v3D(v3D_type) pom = v1; n--;

    while(n--)
        pom = v3D_mul(v3D_type)(pom, va_arg ( arguments, v3D(v3D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v3D_mulCross(tip)(a,b); */
v3D(v3D_type) v3D_mulCross(v3D_type)     (const v3D(v3D_type) v1, const v3D(v3D_type) v2) {
    v3D(v3D_type) pom;
    pom.x = v1.y * v2.z - v1.z * v2.y;
    pom.y = v1.z * v2.x - v1.x * v2.z;
    pom.z = v1.x * v2.y - v1.y * v2.x;
    return pom;
}

/* Funckija za deljenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v3D_div(tip)(a,b); */
v3D(v3D_type) v3D_div(v3D_type)     (const v3D(v3D_type) v1, const v3D(v3D_type) v2) {
    v3D(v3D_type) pom;
    pom.x = v1.x / v2.x;
    pom.y = v1.y / v2.y;
    pom.z = v1.z / v2.z;
    return pom;
}

/* Funckija za sabiranje vektora sa konstantom istog tipa. Poziva se sa: v3D_addc(tip)(a,c); */
v3D(v3D_type) v3D_addc(v3D_type)    (const v3D(v3D_type) v, const v3D_type c) {
    v3D(v3D_type) pom;
    pom.x = v.x + c;
    pom.y = v.y + c;
    pom.z = v.z + c;
    return pom;
}

/* Funckija za oduzimanje konstante od vektora istog tipa. Poziva se sa: v3D_subc(tip)(a,c); */
v3D(v3D_type) v3D_subc(v3D_type)    (const v3D(v3D_type) v, const v3D_type c) {
    v3D(v3D_type) pom;
    pom.x = v.x - c;
    pom.y = v.y - c;
    pom.z = v.z - c;
    return pom;
}

/* Funckija za mnozenje vektora konstantom istog tipa. Poziva se sa: v3D_mulc(tip)(a,c); */
v3D(v3D_type) v3D_mulc(v3D_type)    (const v3D(v3D_type) v, const v3D_type c) {
    v3D(v3D_type) pom;
    pom.x = v.x * c;
    pom.y = v.y * c;
    pom.z = v.z * c;
    return pom;
}

/* Funckija za deljenje vektora konstantom istog tipa. Poziva se sa: v3D_divc(tip)(a,c); */
v3D(v3D_type) v3D_divc(v3D_type)    (const v3D(v3D_type) v, const v3D_type c) {
    v3D(v3D_type) pom;
    pom.x = v.x / c;
    pom.y = v.y / c;
    pom.z = v.z / c;
    return pom;
}

/* Funckija za modul vektora */
double v3D_modulus(v3D_type)  (const v3D(v3D_type) v) {
    double x = (double) v.x;
    double y = (double) v.y;
    double z = (double) v.z;
    return sqrt(x*x+y*y+z*z);
}

/* Funckija za normalizaciju vektora na ort */
v3D(v3D_type) v3D_normalize(v3D_type)  (const v3D(v3D_type) v) {
    double mod = v3D_modulus(v3D_type)(v);
    double x = (double) v.x / mod;
    double y = (double) v.y / mod;
    double z = (double) v.z / mod;
    return (v3D(v3D_type)){(v3D_type)x, (v3D_type)y, (v3D_type)z};
}
