/*!
 * \file    vector2D.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of quasi-template structure of 2D vector and functions for it's manipulation
 *
 * Ovaj fajl je namenjen iskljucivo za kvazigenericko
 * kreiranje tipova vektora. Upotreba za pravljenje tipa
 * vektora odredjenog tipa je sledeca:
 *
 *          #define v2D_type TIP
 *          #include "vector2D.h"
 *          #undef v2D_type
 *
 * Tip koji se koristi mora imati definisane operatore:
 *
 *          +, -, *, /, =
 *
 * Ako je pri kreiranju tipa definisan makro
 *
 *          #define v2D_custom_allignment BROJ
 *
 * svi elementi strukture se allign-uju na BROJ, pa
 * to resava problem cast-a pokazivaca na vektore
 * razlicitog tipa, ali se mogu javiti problemi ako
 * se cast vrsi iz manjeg u veci tip i naravno, nije
 * moguce vrsiti cast int <-> float i slicno. Ovo radi
 * na GNU (GCC) i Microsoft (MSVC) C komajlerima
*/

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <math.h>
#include <stdint.h>
#include <stdarg.h>

/* ****************************************************************************
*           MACROS
******************************************************************************/

#ifndef v2D

    /* Pomocni makro resava problem razresavanja makroa kada se koristi ## */
    #define _pom_v2D(type) v2D_##type
    #define v2D(type) _pom_v2D(type)

    /* Makro za template konverziju vecktora. Poziva se sa: v2D_cast(novi_tip, vektor) */
    #define v2D_cast(type_to, A) (v2D(type_to)){(type_to)A.x,(type_to)A.y}

    /* Sledeci makroi sluze za pozivanje funkcija navedenih ispod. Poziv je opisan u komentaru svake funckije. */

    #define _pom_v2D_add(type) v2D_add_##type
    #define v2D_add(type) _pom_v2D_add(type)

    /* EXPERIMENTAL */
    #define _pom_v2D_addn(type) v2D_addn_##type
    #define v2D_addn(type) _pom_v2D_addn(type)

    #define _pom_v2D_sub(type) v2D_sub_##type
    #define v2D_sub(type) _pom_v2D_sub(type)

    #define _pom_v2D_mul(type) v2D_mul_##type
    #define v2D_mul(type) _pom_v2D_mul(type)

    /* EXPERIMENTAL */
    #define _pom_v2D_muln(type) v2D_muln_##type
    #define v2D_muln(type) _pom_v2D_muln(type)

    #define _pom_v2D_div(type) v2D_div_##type
    #define v2D_div(type) _pom_v2D_div(type)

    #define _pom_v2D_addc(type) v2D_addc_##type
    #define v2D_addc(type) _pom_v2D_addc(type)

    #define _pom_v2D_subc(type) v2D_subc_##type
    #define v2D_subc(type) _pom_v2D_subc(type)

    #define _pom_v2D_mulc(type) v2D_mulc_##type
    #define v2D_mulc(type) _pom_v2D_mulc(type)

    #define _pom_v2D_divc(type) v2D_divc_##type
    #define v2D_divc(type) _pom_v2D_divc(type)

    #define _pom_v2D_modulus(type) v2D_modulus_##type
    #define v2D_modulus(type) _pom_v2D_modulus(type)

#endif // v2D

/* ****************************************************************************
*           2D VECTOR STRUCTURE
******************************************************************************/

/*!
 * \struct v2D
 * \brief Quasi-template 2D vector. Call it with: v2D(TYPE)
 */

#ifdef v2D_custom_allignment

    #if defined(__GNUC__)

        typedef struct {
            v2D_type x __attribute__ ((aligned (v2D_custom_allignment))); /**< x coordinate */
            v2D_type y __attribute__ ((aligned (v2D_custom_allignment))); /**< y coordinate */
        } v2D(v2D_type);

    #elif defined(_MSC_VER)

        typedef struct {
            __declspec(align(v2D_custom_allignment)) v2D_type x;  /**< x coordinate */
            __declspec(align(v2D_custom_allignment)) v2D_type y;  /**< y coordinate */
        } v2D(v2D_type);

    #else

        #error Do not know how to setup allignment with your compiler

    #endif // complier checking

#else

    typedef struct {
        v2D_type x; /**< x coordinate */
        v2D_type y; /**< y coordinate */
        /*
            mogli bi ovde da se stave pokazivaci na svaku od tih funckija,
            ali prvo sto bi zauzimali dodatnu memoriju, drugo sto bi
            trebalo dodeliti im pokazivace na ove implementirane funckije
            prilikom inicijalizacije, a opet ne bi bilo toliko lepo jer ispada
            da imamo v1 = v1.add(v1,v2) sto je visak ponavaljanja v1 za v1+=v2
            ili v1 = v1+v2
        */
    } v2D(v2D_type);

#endif

/* ***********************************************************************************************************************
*           FUNCTIONS
*************************************************************************************************************************/

/* Funckija za sabiranje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v2D_add(tip)(a,b); */
v2D(v2D_type) v2D_add(v2D_type)     (const v2D(v2D_type) v1, const v2D(v2D_type) v2) {
    v2D(v2D_type) pom;
    pom.x = v1.x + v2.x;
    pom.y = v1.y + v2.y;
    return pom;
}

/* Funckija za sabiranje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v2D_addn(tip)(n,a1,a2,a3,..,an); */
v2D(v2D_type) v2D_addn(v2D_type)     (uint8_t n, ... ) {

    va_list arguments;
    va_start (arguments, n);

    v2D(v2D_type) pom = {0,0};

    while(n--)
        pom = v2D_add(v2D_type)(pom, va_arg ( arguments, v2D(v2D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za oduzimanje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v2D_sub(tip)(a,b); */
v2D(v2D_type) v2D_sub(v2D_type)     (const v2D(v2D_type) v1, const v2D(v2D_type) v2) {
    v2D(v2D_type) pom;
    pom.x = v1.x - v2.x;
    pom.y = v1.y - v2.y;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v2D_mul(tip)(a,b); */
v2D(v2D_type) v2D_mul(v2D_type)     (const v2D(v2D_type) v1, const v2D(v2D_type) v2) {
    v2D(v2D_type) pom;
    pom.x = v1.x * v2.x;
    pom.y = v1.y * v2.y;
    return pom;
}

/* Funckija za mnozenje odgovarajucih elemenata n vektora istog tipa. Poziva se sa: v2D_muln(tip)(n,a1,a2,a3,..,an); */
v2D(v2D_type) v2D_muln(v2D_type)     (uint8_t n, v2D(v2D_type) v1, ... ) {
    va_list arguments;
    va_start (arguments, v1);

    v2D(v2D_type) pom = v1; n--;

    while(n--)
        pom = v2D_mul(v2D_type)(pom, va_arg ( arguments, v2D(v2D_type) ));

    va_end (arguments);

    return pom;
}

/* Funckija za deljenje odgovarajucih elemenata dva vektora istog tipa. Poziva se sa: v2D_div(tip)(a,b); */
v2D(v2D_type) v2D_div(v2D_type)     (const v2D(v2D_type) v1, const v2D(v2D_type) v2) {
    v2D(v2D_type) pom;
    pom.x = v1.x / v2.x;
    pom.y = v1.y / v2.y;
    return pom;
}

/* Funckija za sabiranje vektora sa konstantom istog tipa. Poziva se sa: v2D_addc(tip)(a,c); */
v2D(v2D_type) v2D_addc(v2D_type)    (const v2D(v2D_type) v, const v2D_type c) {
    v2D(v2D_type) pom;
    pom.x = v.x + c;
    pom.y = v.y + c;
    return pom;
}

/* Funckija za oduzimanje konstante od vektora istog tipa. Poziva se sa: v2D_subc(tip)(a,c); */
v2D(v2D_type) v2D_subc(v2D_type)    (const v2D(v2D_type) v, const v2D_type c) {
    v2D(v2D_type) pom;
    pom.x = v.x - c;
    pom.y = v.y - c;
    return pom;
}

/* Funckija za mnozenje vektora konstantom istog tipa. Poziva se sa: v2D_mulc(tip)(a,c); */
v2D(v2D_type) v2D_mulc(v2D_type)    (const v2D(v2D_type) v, const v2D_type c) {
    v2D(v2D_type) pom;
    pom.x = v.x * c;
    pom.y = v.y * c;
    return pom;
}

/* Funckija za deljenje vektora konstantom istog tipa. Poziva se sa: v2D_divc(tip)(a,c); */
v2D(v2D_type) v2D_divc(v2D_type)    (const v2D(v2D_type) v, const v2D_type c) {
    v2D(v2D_type) pom;
    pom.x = v.x / c;
    pom.y = v.y / c;
    return pom;
}

/* Funckija za modul vektora */
double v2D_modulus(v2D_type)  (const v2D(v2D_type) v) {
    double x = (double) v.x;
    double y = (double) v.y;
    return sqrt(x*x+y*y);
}

