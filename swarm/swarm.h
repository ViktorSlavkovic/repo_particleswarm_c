/*!
 * \file    swarm.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Includes swarm2D.h and swarm3D.h
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef SWARM_H_INCLUDED
#define SWARM_H_INCLUDED

#include "swarm2D.h"
#include "swarm3D.h"

#endif // SWARM_H_INCLUDED
