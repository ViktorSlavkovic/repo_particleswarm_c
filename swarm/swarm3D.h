/*!
 * \file    swarm3D.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of swarm3D structure and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef SWARM_3D_H_INCLUDED
#define SWARM_3D_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <SFMT.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "../math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

/* ****************************************************************************
*           SWARM STRUCTURE
******************************************************************************/

/*!
 * \struct swarm3D
 * \brief  Structure that describes swarm - it's particles and parameters
 */
typedef struct {

    v3D(float) * x;             /*!< \brief Array of position vectors for each particle */
    v3D(float) * v;             /*!< \brief Array of velocity vectors for each particle */
    v3D(float) * pb;            /*!< \brief Personal bests - Array of vectors of best points that every particle explored on it's own*/
    v3D(float) gb;              /*!< \brief Global best - The best of personal bests */
    float      gb_val;          /*!< \brief f_cost(gb) */

    uint64_t n;                 /*!< \brief Swarm size */

    float c_cognitive;          /*!< \brief Cognitive coefficient */
    float c_social;             /*!< \brief Social coefficient */
    float c_w;                  /*!< \brief Inertial coefficient */
    float c_vmax;               /*!< \brief Maximal velocity */

    v3D(float) min_bound;       /*!< \brief Minimal coordinates of optimization space */
    v3D(float) max_bound;       /*!< \brief Maximal coordinates of optimization space */

    float (*f_cost)(const v3D(float), float); /*!< \brief Pointer to cost function */

    float t;                    /*!< \brief Current swarm time */

    sfmt_t sfmt;                /*!< \brief Mersenne Twister structure */

} swarm3D;

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Constructor-like function
 *  \param cost         Pointer to cost function
 *  \param n            Swarm size
 *  \param v_max        Maximal velocity
 *  \param min_bound    Minimal coordinates of optimization space
 *  \param max_bound    Maximal coordinates of optimization space
 *  \return The pointer to newly created swarm2D instance
 */
swarm3D * swarm3D_create    (   float (*cost)(const v3D(float), float),
                                uint64_t n,
                                float v_max,
                                v3D(float) min_bound,
                                v3D(float) max_bound
                            )
{

    swarm3D * s;
    s = (swarm3D*)malloc(sizeof(swarm3D));

    s->f_cost = cost;

    s->n=n;
    s->c_cognitive = 1.494;
    s->c_social = 1.494;
    s->c_w = 0.729;

    s->c_vmax=v_max;

    s->min_bound = min_bound;
    s->max_bound = max_bound;

    s->t = 0.0;

    /* srand(time(0)); */
    sfmt_init_gen_rand(&(s->sfmt), time(0));

    s->x = (v3D(float)*) calloc(n,sizeof(v3D(float)));
    uint64_t i;
    for (i=0; i<n; i++) {
        s->x[i].x = (float) sfmt_genrand_res53(&(s->sfmt))*(max_bound.x-min_bound.x)+min_bound.x;
        s->x[i].y = (float) sfmt_genrand_res53(&(s->sfmt))*(max_bound.y-min_bound.y)+min_bound.y;
        s->x[i].z = (float) sfmt_genrand_res53(&(s->sfmt))*(max_bound.z-min_bound.z)+min_bound.z;
    }

    s->v = (v3D(float)*) calloc(n,sizeof(v3D(float)));

    s->pb = (v3D(float)*) calloc(n,sizeof(v3D(float)));
    for (i=0; i<n; i++) s->pb[i]=s->x[i];

    s->gb_val = s->f_cost(s->x[0], s->t);
    s->gb = s->x[0];

    for (i=1; i<n; i++) {
        float pom = s->f_cost(s->x[i], s->t);
        if (pom < s->gb_val) {
            s->gb_val = pom;
            s->gb = s->x[i];
        }
    }

    return s;
}

/*!
 *  \brief Update function
 *  \param s            Pointer to swarm3D instance
 *  \param dt           Time that has passed since last update
 *  \param rand_w       Parameter that tells update function should it multiply inertia coefficient with random number from (0,1) range
 *  \param same_rand    Parameter that tells update function should it use all the same random numbers
 */
void swarm3D_update         (   swarm3D * s,
                                float dt,
                                int8_t /*boolean*/ rand_w,
                                int8_t /*boolean*/ same_rand
                            )
{
    s->t += dt;

    uint64_t i;
    for (i=0; i < s->n; i++) {

        v3D(float) old = s->x[i];

        float rand_1, rand_2, rand_3;
        if (same_rand) rand_1 = rand_2 = rand_3 = (float) sfmt_genrand_res53(&(s->sfmt));
        else {
            rand_1 = (float) sfmt_genrand_res53(&(s->sfmt));
            rand_2 = (float) sfmt_genrand_res53(&(s->sfmt));
            rand_3 = (float) sfmt_genrand_res53(&(s->sfmt));
        }

        v3D(float) pom_1  =   v3D_mulc(float)(
                                        s->v[i],
                                        s->c_w * ( (rand_w) ? rand_1: 1 )
                                    );
        v3D(float) pom_2  =   v3D_sub(float)(
                                        s->pb[i],
                                        s->x[i]
                                    );
                         pom_2  =   v3D_mulc(float)(
                                        pom_2,
                                        s->c_cognitive * rand_2
                                    );

        v3D(float) pom_3  =   v3D_sub(float)(
                                        s->gb,
                                        s->x[i]
                                    );
                         pom_3  =   v3D_mulc(float)(
                                        pom_3,
                                        s->c_social * rand_3
                                    );

        s->v[i] = v3D_addn(float)(3,pom_1,pom_2,pom_3);

        float mod = v3D_modulus(float)(s->v[i]);

        if (mod > s->c_vmax)
            s->v[i] = v3D_mulc(float)(s->v[i], s->c_vmax/mod);

        s->x[i] = v3D_add(float)(s->x[i], v3D_mulc(float)(s->v[i], dt));

        float px=s->x[i].x;
        float py=s->x[i].y;
        float pz=s->x[i].z;

        if (px<s->min_bound.x) s->x[i].x = 2*s->min_bound.x-px, s->v[i].x*=(-1);
        if (px>s->max_bound.x) s->x[i].x = 2*s->max_bound.x-px, s->v[i].x*=(-1);
        if (py<s->min_bound.y) s->x[i].y = 2*s->min_bound.y-py, s->v[i].y*=(-1);
        if (py>s->max_bound.y) s->x[i].y = 2*s->max_bound.y-py, s->v[i].y*=(-1);
        if (pz<s->min_bound.z) s->x[i].z = 2*s->min_bound.z-pz, s->v[i].z*=(-1);
        if (pz>s->max_bound.z) s->x[i].z = 2*s->max_bound.z-pz, s->v[i].z*=(-1);

        float pom = s->f_cost(s->x[i], s->t);

        if (pom < s->f_cost(old, s->t)) s->pb[i] = s->x[i];
        if (pom < s->gb_val) {
            s->gb_val = pom;
            s->gb = s->x[i];
        }

    }
}

/*!
 *  \brief Destructor-like function
 *  \param s            Pointer to swarm3D instance
 */
void swarm3D_destroy        (   swarm3D * s )
{
    free(s->x);
    free(s->v);
    free(s->pb);
    free(s);
    s=NULL;
}

#endif // SWARM_3D_H_INCLUDED
