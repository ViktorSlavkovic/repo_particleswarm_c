# Visualizing Particle Swarm in 2D and 3D #

### About ###
>This software visualizes performance of Particle Swarm algorithm in 2D and 3D.

### Programming Language ###
> This software is written is entirely written in C programming language (standard C99).

### To Do: ###
* Improve documentation for math module
* Improve documentation for transform module
* Split the program into several threads (preferably with C11)

### Dependencies###
* SDL 2.0.3     (should work with 2.0 and above)
* OpenGL 3.1    (any version of modern OpenGL) 
* GLEW 1.6      (1.5.1 and above according to OpenGL version)

### Author ###
> Viktor Slavkovic : [viktor.slavkovic@gmail.com](mailto:viktor.slavkovic@gmail.com) 

### License ###
>    This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU General Public License as published by
>    the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>    
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU General Public License for more details.
>
>    You should have received a copy of the GNU General Public License
>    along with this program.  If not, see <http://www.gnu.org/licenses/>.