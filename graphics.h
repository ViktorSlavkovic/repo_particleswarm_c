/*!
 * \file    graphics.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of graphics structure and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <stdint.h>

#include <SDL2/SDL.h>
#include <GL/glew.h>

#include "shaders/shader.h"
#include "transform/transform.h"

#ifndef MATH_INT_DEFINED
    #define math_type int
    #include "math/math_includer.h"
    #undef math_type
    #define MATH_INT_DEFINED
#endif

/* ****************************************************************************
*           GRAPHICS STRUCTURE
******************************************************************************/

/*!
 * \struct graphics
 * \brief  Structure for describing SDL graphics (window, renderer, texture, etc.)
 */
typedef struct {

    int w;                      /*!< \brief Screen width */
    int h;                      /*!< \brief Screen height */

    SDL_Window * _window;       /*!< \brief Pointer to SDL_Window */
    SDL_GLContext _context;     /*!< \brief Context for OpenGL */

    GLint _shader;              /*!< \brief "Pointer" to shader program */

    GLuint shader_RGBA;         /*!< \brief "Pointer" to uniform variable in fragment shader */
    GLuint shader_Transform;    /*!< \brief "Pointer" to uniform variable in vertex shader */

    t_camera camera;            /*!< \brief Camera info */
    t_perspective perspective;  /*!< \brief Perspective projection info */

} graphics;

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Initializes graphics and opens window
 *  \param G            Pointer to graphics instance
 *  \param force_FPS    Should program force FPS to maximum?
 *  \param color        3D vector of floats (R,G,B) in range [0,1]
 */
void graphics_Init(graphics * G, int8_t /*boolean*/ force_FPS, v3D(float) color) {

    /* VIDEO PODSISTEM*/
    if (SDL_Init(SDL_INIT_VIDEO)) {
        printf("SDL Error: \"%s\" \n", SDL_GetError());
        exit(-1);
    }
    printf("SDL Success: Video subsystem initialized.\n");

    /* REZOLUCIJA */
    SDL_DisplayMode current_display;
    SDL_GetCurrentDisplayMode(0, &current_display);
    G->w = current_display.w;
    G->h = current_display.h;

    printf("    Resolution is: %d x %d\n", G->w, G->h);

    /* SDL - OpenGL opcije */
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    /* PROZOR */
    G->_window = SDL_CreateWindow(  "OpenGL",
                                    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, G->w, G->h,
                                    SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_SHOWN |SDL_WINDOW_FULLSCREEN );

    if (! G->_window) {
        printf("SDL Error: %s\n", SDL_GetError());
        SDL_Quit();
        exit(-1);
    }
    printf("SDL Success: Window created.\n");

    /* OpenGL kontekst */
    G->_context = SDL_GL_CreateContext(G->_window);
    if (! G->_context) {
        printf("SDL Error: %s\n", SDL_GetError());
        SDL_DestroyWindow(G->_window);
        SDL_Quit();
        exit(-1);
    }
    printf("SDL Success: OpenGL context created.\n");

    /* GLEW */
    glewExperimental = GL_TRUE; /* Nije obavezno */
    GLenum res = glewInit();
    if (res != GLEW_OK) {
        printf("GLEW Error: %s\n", glewGetErrorString(res));
        SDL_GL_DeleteContext(G->_context);
        SDL_DestroyWindow(G->_window);
        SDL_Quit();
        exit(-1);
    }
    printf("GLEW(%s) Success: Initialized.\n", glewGetString(GLEW_VERSION));

    SDL_ShowCursor(0);

    if (!force_FPS) SDL_GL_SetSwapInterval(1);

    glClearColor(color.x, color.y, color.z, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
}

/*!
 *  \brief Closes window and cleans up graphics
 *  \param G            Pointer to graphics instance
 */
void graphics_UnInit(graphics * G) {
    SDL_GL_DeleteContext(G->_context);
    SDL_DestroyWindow(G->_window);
    SDL_Quit();
}

/*!
 *  \brief Moves cursor to the center of the screen
 */
void graphics_CenterMouse(const graphics * G) {
    int w, h;
    SDL_GetWindowSize(G->_window, &w, &h);
    SDL_WarpMouseInWindow(G->_window, w/2, h/2);
}

/*!
 *  \brief Initializes shader program
 *  \param G            Pointer to graphics instance
 */
void graphics_InitShader(graphics * G) {
    G->_shader = glCreateProgram();
    if (G->_shader) {
        int res =     !addShader(G->_shader, "shaders/transform.vs", GL_VERTEX_SHADER)
                    & !addShader(G->_shader, "shaders/rgba.fs", GL_FRAGMENT_SHADER)
                    & !linkShaderProgram(G->_shader);
        if (!res) {
            printf("Error: Cannot load shader.");
            exit(-1);
        }
    }
    G->shader_Transform = glGetUniformLocation(G->_shader, "T");
    G->shader_RGBA = glGetUniformLocation(G->_shader, "RGBA");

    glUseProgram(G->_shader);
}

/*!
 *  \brief Sends matrix to vertex shader
 *  \param G            Pointer to graphics instance
 *  \param matrix       Pointer to matrix to send
 */
void graphics_PushMatrix(const graphics * G, const m4D(float) * matrix) {
    glUniformMatrix4fv(G->shader_Transform, 1, GL_TRUE, (const GLfloat*) matrix);
}

/*!
 *  \brief Refresh screen (swap buffer and clear)
 *  \param G            Pointer to graphics instance
 */
void graphics_Refresh(const graphics * G) {
    SDL_GL_SwapWindow(G->_window);
    glClear(GL_COLOR_BUFFER_BIT);
}

#endif // GRAPHICS_H_INCLUDED
