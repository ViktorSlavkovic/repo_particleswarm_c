/*!
 * \file    shader.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Shader manipulation functions
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#include <GL/glew.h>

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Adds shader from GLSL source to shader program
 *  \param shader_program   "Pointer" to shader program
 *  \param src_path         Path to GLSL shader source
 *  \param shader_type      Type of shader
 *  \return Zero on success, nonzero on failure
 */
int addShader(GLuint shader_program, const char* src_path, GLenum shader_type);

/*!
 *  \brief Links shader program
 *  \param shader_program   "Pointer" to shader program
 *  \return Zero on success, nonzero on failure
 */
int linkShaderProgram(GLuint shader_program);

#endif // SHADER_H_INCLUDED
