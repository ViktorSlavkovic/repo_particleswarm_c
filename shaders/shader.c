/*!
 * \file    shader.c
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Implements functions from shader.h
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#include "shader.h"

int addShader(GLuint shader_program, const char* src_path, GLenum shader_type) {

    GLuint shader_obj = glCreateShader(shader_type);
    if (!shader_obj) return -1;

    /* PREBROJ karaktere */
    FILE* fin = fopen(src_path, "r");
    if (!fin) {
        printf("Error: Unable to read shader file: %s\n", src_path);
        return -1;
    }

    GLint num = 0;

    while(getc(fin)!=EOF)
        num++;

    fclose(fin);

    /* CITAJ */
    GLchar* src_array[1] = {(GLchar*)calloc(num,sizeof(GLchar))};
    GLint lenth_array[1] = {num};

    fin = fopen(src_path, "r");
    int i = 0, c;
    while((c = getc(fin))!=EOF) src_array[0][i++]=c;

    fclose(fin);

    glShaderSource(shader_obj, 1, (const GLchar**) src_array, (const GLint*) lenth_array);
    glCompileShader(shader_obj);

    free(src_array[0]);

    GLint success;
    glGetShaderiv(shader_obj, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLchar msg[1024];
        glGetShaderInfoLog(shader_obj, 1024, 0, msg);
        printf("Error compiling shader type %d: %s\n", shader_type, msg);
        return -1;
    }

    glAttachShader(shader_program, shader_obj);

    return 0;
}

int linkShaderProgram(GLuint shader_program) {

    GLint success;

    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        GLchar msg[1024];
        glGetProgramInfoLog(shader_program, 1024, 0, msg);
        printf("Error linking shader program: %s\n", msg);
        return -1;
    }

    glValidateProgram(shader_program);
    glGetProgramiv(shader_program, GL_VALIDATE_STATUS, &success);
    if (!success) {
        GLchar msg[1024];
        glGetProgramInfoLog(shader_program, 1024, 0, msg);
        printf("Error validating shader program: %s\n", msg);
        return -1;
    }

    return 0;
}
