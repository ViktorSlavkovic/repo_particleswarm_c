#version 330

out vec4 FragColor;

uniform vec4 RGBA;

void main() {
    FragColor = vec4(clamp(RGBA,0,1));
}
