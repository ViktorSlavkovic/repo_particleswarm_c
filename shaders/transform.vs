#version 330

layout (location = 0) in vec3 POS;

uniform mat4 T;
out vec4 Color;

void main() {
    gl_Position = T * vec4(POS, 1.0);
	Color = vec4(clamp(POS, 0.0, 1.0), 1.0);
}
