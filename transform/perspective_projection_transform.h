/*!
 * \file    perspective_projection_transform.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of perspective projection structure and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef PERSPECTIVE_PROJECTION_TRANSFORM_H_INCLUDED
#define PERSPECTIVE_PROJECTION_TRANSFORM_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <GL/glew.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "../math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

/* ****************************************************************************
*           PERSPECTIVE PROJECTION STRUCTURE
******************************************************************************/

/*!
 * \struct t_perspective
 * \brief  Structure that describes perspective projection
 */
typedef struct {
    float fov;      /*!< \brief Field Of View - View angle */
    float ar;       /*!< \brief Aspect ration of the screen - W:H  */
    float z_near;   /*!< \brief Near plane Z */
    float z_far;    /*!< \brief Far plane Z */
} t_perspective;

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

void initPerspectiveProjection(const t_perspective * P, m4D(float)* result) {
    float tgHalfFOV = tanf(P->fov/2);
    float z_range = P->z_near-P->z_far;

    (*result)[0] = (v4D(float)){1 / (P->ar*tgHalfFOV),     0,                          0,                              0};
    (*result)[1] = (v4D(float)){0,                      1/tgHalfFOV,                0,                              0};
    (*result)[2] = (v4D(float)){0,                      0,    -(P->z_near+P->z_far)/z_range,      2.0f * (P->z_near) * (P->z_far)/z_range};
    (*result)[3] = (v4D(float)){0,                      0,                          1,                              0};
}

void concatRightPerspectiveProjection(const t_perspective * P, m4D(float)* result) {
    m4D(float) pom;
    initPerspectiveProjection(P, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftPerspectiveProjection(const t_perspective * P, m4D(float)* result) {
    m4D(float) pom;
    initPerspectiveProjection(P, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

#endif // PERSPECTIVE_PROJECTION_TRANSFORM_H_INCLUDED
