/*!
 * \file    camera_transform.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of camera structure and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef CAMERA_TRANSFORM_H_INCLUDED
#define CAMERA_TRANSFORM_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <GL/glew.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "../math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

#include "world_transform.h"

/* ****************************************************************************
*           CAMERA STRUCTURE
******************************************************************************/

/*!
 * \struct t_camera
 * \brief  Structure that describes camera orientation and position
 */
typedef struct {
    v3D(float)  up;         /*!< \brief Vector that points up */
    v3D(float)  target;     /*!< \brief Vector that point to target */
    v3D(float)  pos;        /*!< \brief Camera position vector */
} t_camera;

/* ****************************************************************************
*           AUXILIARY FUNCTIONS
******************************************************************************/

void initCameraTranslation(v3D(float) v, m4D(float)* result) {
    (*result)[0] = (v4D(float)){1, 0, 0, -v.x};
    (*result)[1] = (v4D(float)){0, 1, 0, -v.y};
    (*result)[2] = (v4D(float)){0, 0, 1, -v.z};
    (*result)[3] = (v4D(float)){0, 0, 0,  1 };
}

void concatRightCameraTranslation(v3D(float) v, m4D(float)* result) {
    m4D(float) pom;
    initCameraTranslation(v, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftCameraTranslation(v3D(float) v, m4D(float)* result) {
    m4D(float) pom;
    initCameraTranslation(v, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

void initCameraRotation(v3D(float) up, v3D(float) target, m4D(float)* result) {
    v3D(float) N = v3D_normalize(float)(target);
    v3D(float) U = v3D_normalize(float)(up);
    U = v3D_mulCross(float)(N,U);
    v3D(float) V = v3D_mulCross(float)(U,N);

    (*result)[0] = (v4D(float)){U.x, U.y, U.z, 0};
    (*result)[1] = (v4D(float)){V.x, V.y, V.z, 0};
    (*result)[2] = (v4D(float)){N.x, N.y, N.z, 0};
    (*result)[3] = (v4D(float)){  0,   0,   0, 1};
}

 void fixNormalizeUpToTarget(v3D(float) *up, v3D(float) *target) {
    v3D(float) right =  v3D_mulCross(float)(*target,*up);
    *up = v3D_mulCross(float)(right,*target);
    *up = v3D_normalize(float)(*up);
    *target = v3D_normalize(float)(*target);
 }

void concatRightCameraRotation(v3D(float) up, v3D(float) target, m4D(float)* result) {
    m4D(float) pom;
    initCameraRotation(up, target, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftCameraRotation(v3D(float) up, v3D(float) target, m4D(float)* result) {
    m4D(float) pom;
    initCameraRotation(up, target, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

void setCameraRotation(v3D(float) up, v3D(float) target, t_camera * camera) {
    camera->up = up;
    camera->target = target;
    fixNormalizeUpToTarget(&camera->up, &camera->target);
}

void setCameraTranslation(v3D(float) v, t_camera * camera) {
    camera->pos = v;
}

void moveCamera(v3D(float) v, t_camera * camera) {
    camera->pos = v3D_add(float) (camera->pos, v);
}

void rotateLeftCamera(float angle, t_camera * camera) {

    Q(float) qrot = { cosf(angle/2.0f), v3D_mulc(float)(camera->up, sinf(angle/2.0f)) };
    Q(float) cqrot = Q_conjugate(float)(qrot);
    Q(float) target = {0, camera->target};

    Q(float) pom;
    pom = Q_mulHamilton(float)(qrot, target);
    pom = Q_mulHamilton(float)(pom, cqrot);

    camera->target = pom.Im;
}

void rotateRightCamera(float angle, t_camera * camera) {
    Q(float) qrot = { cosf(angle/2.0f), v3D_mulc(float)((v3D(float)){-(camera->up).x,-(camera->up).y,-(camera->up).z}, sinf(angle/2.0f)) };
    Q(float) cqrot = Q_conjugate(float)(qrot);
    Q(float) target = {0, camera->target};

    Q(float) pom;
    pom = Q_mulHamilton(float)(qrot, target);
    pom = Q_mulHamilton(float)(pom, cqrot);

    camera->target = pom.Im;
}

void rotateUpCamera(float angle, t_camera * camera) {

    v3D(float) right = v3D_mulCross(float)(camera->target, camera->up);
    Q(float) qrot = { cosf(angle/2.0f), v3D_mulc(float)(right, sinf(angle/2.0f)) };
    Q(float) cqrot = Q_conjugate(float)(qrot);
    Q(float) target = {0, camera->target};

    Q(float) pom;
    pom = Q_mulHamilton(float)(qrot, target);
    pom = Q_mulHamilton(float)(pom, cqrot);

    camera->target = pom.Im;

    fixNormalizeUpToTarget(&camera->up, &camera->target);
}

void rotateDownCamera(float angle, t_camera * camera) {
    v3D(float) left = v3D_mulCross(float)(camera->up, camera->target);
    Q(float) qrot = { cosf(angle/2.0f), v3D_mulc(float)(left, sinf(angle/2.0f)) };
    Q(float) cqrot = Q_conjugate(float)(qrot);
    Q(float) target = {0, camera->target};

    Q(float) pom;
    pom = Q_mulHamilton(float)(qrot, target);
    pom = Q_mulHamilton(float)(pom, cqrot);

    camera->target = pom.Im;

    fixNormalizeUpToTarget(&camera->up, &camera->target);
}

void getCameraTransform(const t_camera * camera, m4D(float)* result) {
    initCameraRotation(camera->up, camera->target, result);
    concatRightCameraTranslation(camera->pos, result);
}

#endif // CAMERA_TRANSFORM_H_INCLUDED
