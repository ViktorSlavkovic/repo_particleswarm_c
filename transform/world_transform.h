/*!
 * \file    world_transform.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Generating world transformations
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef WORLD_TRANSFORM_H_INCLUDED
#define WORLD_TRANSFORM_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <GL/glew.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "../math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

void initTranslation(v3D(float) v, m4D(float)* result) {
    (*result)[0] = (v4D(float)){1, 0, 0, v.x};
    (*result)[1] = (v4D(float)){0, 1, 0, v.y};
    (*result)[2] = (v4D(float)){0, 0, 1, v.z};
    (*result)[3] = (v4D(float)){0, 0, 0,  1 };
}

void concatRightTranslation(v3D(float) v, m4D(float)* result) {
    m4D(float) pom;
    initTranslation(v, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftTranslation(v3D(float) v, m4D(float)* result) {
    m4D(float) pom;
    initTranslation(v, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

void initRotation(v3D(float) a_rad, m4D(float)* result) {

    m4D_identity(float)(result);
    m4D(float) pom;

    //z
    pom[0] = (v4D(float)){cosf(a_rad.z), -sinf(a_rad.z), 0, 0};
    pom[1] = (v4D(float)){sinf(a_rad.z),  cosf(a_rad.z), 0, 0};
    pom[2] = (v4D(float)){0, 0, 1, 0};
    pom[3] = (v4D(float)){0, 0, 0, 1};

    m4D_mulCross(float)(*result, pom, result);

    //y
    pom[0] = (v4D(float)){cosf(a_rad.y), 0, -sinf(a_rad.y), 0};
    pom[1] = (v4D(float)){0, 1, 0, 0};
    pom[2] = (v4D(float)){sinf(a_rad.y), 0,  cosf(a_rad.y), 0};
    pom[3] = (v4D(float)){0, 0, 0, 1};

    m4D_mulCross(float)(*result, pom, result);

    //x
    pom[0] = (v4D(float)){1, 0, 0, 0};
    pom[1] = (v4D(float)){0, cosf(a_rad.x), -sinf(a_rad.x), 0};
    pom[2] = (v4D(float)){0, sinf(a_rad.x),  cosf(a_rad.x), 0};
    pom[3] = (v4D(float)){0, 0, 0, 1};

    m4D_mulCross(float)(*result, pom, result);
}

void concatRightRotation(v3D(float) a_rad, m4D(float)* result) {
    m4D(float) pom;
    initRotation(a_rad, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftRotation(v3D(float) a_rad, m4D(float)* result) {
    m4D(float) pom;
    initRotation(a_rad, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

void initScale(v3D(float) vs, m4D(float)* result) {
    (*result)[0] = (v4D(float)){vs.x, 0, 0, 0};
    (*result)[1] = (v4D(float)){0, vs.y, 0, 0};
    (*result)[2] = (v4D(float)){0, 0, vs.z, 0};
    (*result)[3] = (v4D(float)){0, 0,  0,  1};
}

void concatRightScale(v3D(float) vs, m4D(float)* result) {
    m4D(float) pom;
    initScale(vs, &pom);
    m4D_mulCross(float)(*result, pom, result);
}

void concatLeftScale(v3D(float) vs, m4D(float)* result) {
    m4D(float) pom;
    initScale(vs, &pom);
    m4D_mulCross(float)(pom, *result, result);
}

#endif // WORLD_TRANSFORM_H_INCLUDED
