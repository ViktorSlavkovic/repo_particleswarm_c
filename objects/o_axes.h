/*!
 * \file    o_axes.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of axes object and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef O_AXES_H_INCLUDED
#define O_AXES_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <GL/glew.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

/* ****************************************************************************
*           GLOBAL VARIABLES
******************************************************************************/

static GLuint o_axes_VBO;   /*!< \brief "Pointer" to vertex buffer object for o_axes */
static GLuint o_axes_IBO;   /*!< \brief "Pointer" to index buffer object for o_axes */

/*! \cond */

static v3D(float) o_axes_Vertices[4] =  {
                                    {0,  0,  0},
                                    { 1000000.0f,  0,  0},
                                    { 0,  1000000.0f,  0},
                                    { 0,  0,  1000000.0f}
                                };

static unsigned int o_axes_Indices[] =   {
                                    0, 1,
                                    0, 2,
                                    0, 3
                                };

/*! \endcond */

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Initializes o_swarm
 */
void o_axes_Create() {

    glGenBuffers(1, &o_axes_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, o_axes_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(o_axes_Vertices), o_axes_Vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &o_axes_IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o_axes_IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(o_axes_Indices), o_axes_Indices, GL_STATIC_DRAW);
}

/*!
 *  \brief Draws o_axes in OpenGL
 *  \param Shader_RGBA  "Pointer" to uniform variable in fragment shader
 */
void o_axes_Draw(GLuint Shader_RGBA) {

    glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, o_axes_VBO);
        //glBufferData(GL_ARRAY_BUFFER, sizeof(o_axes_Vertices), o_axes_Vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o_axes_IBO);
        //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(o_axes_Indices), o_axes_Indices, GL_STATIC_DRAW);

        v4D(float) RGBA;
        GLvoid * pointer;
        glGetVertexAttribPointerv(0,GL_VERTEX_ATTRIB_ARRAY_POINTER,&pointer);

        // x axis
        RGBA = (v4D(float)){1.0, 0.0, 0.0, 0.1};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, pointer+0*4);
        // y axis
        RGBA= (v4D(float)){0.0, 1.0, 0.0, 0.1};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, pointer+2*4);
        // z axis
        RGBA= (v4D(float)){0.0, 0.0, 1.0, 0.1};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);
        glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, pointer+4*4);

    glDisableVertexAttribArray(0);

}

#endif // O_AXES_H_INCLUDED
