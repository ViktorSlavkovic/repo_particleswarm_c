/*!
 * \file    o_swarm.h
 *
 * \author  Viktor Slavkovic
 *
 * \brief   Definition of swarm object and functions for it's manipulation
 *
 * \copyright Copyright 2015 Viktor Slavkovic
 * \license This project is released under the GNU Public License.
*/

#ifndef O_SWARM_H_INCLUDED
#define O_SWARM_H_INCLUDED

/* ****************************************************************************
*           INCLUDES
******************************************************************************/

#include <GL/glew.h>

#ifndef MATH_FLOAT_DEFINED
    #define math_type float
    #include "math/math_includer.h"
    #undef math_type
    #define MATH_FLOAT_DEFINED
#endif

#include "../swarm/swarm.h"

/* ****************************************************************************
*           GLOBAL VARIABLES
******************************************************************************/

static GLuint o_swarm_VBO;  /*!< \brief "Pointer" to vertex buffer object for o_swarm */
static GLuint o_swarm_IBO;  /*!< \brief "Pointer" to index buffer object  for o_swarm */

/*! \cond */

static unsigned int o_swarm_min_indices[] = {
        0, 1,
        0, 2,
        0, 3,
        0, 4
};

static unsigned int o_swarm_bound_indices[] = {
        0, 1,
        0, 4,
        0, 3,
        1, 2,
        1, 5,
        4, 5,
        4, 7,
        3, 7,
        3, 2,
        5, 6,
        7, 6,
        2, 6
};

/*! \endcond */

/* ****************************************************************************
*           FUNCTIONS
******************************************************************************/

/*!
 *  \brief Initializes o_swarm
 *  \param s    Pointer to swarm3D
 */
void o_swarm_Create(const swarm3D * s) {

    glGenBuffers(1, &o_swarm_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, o_swarm_VBO);
    glBufferData(GL_ARRAY_BUFFER, s->n * sizeof(v3D(float)), s->x, GL_DYNAMIC_DRAW);

    glGenBuffers(1, &o_swarm_IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o_swarm_IBO);


}

/*!
 *  \brief Draws o_swarm in OpenGL
 *  \param Shader_RGBA  "Pointer" to uniform variable in fragment shader
 *  \param s            Pointer to cost function
 *  \param f_min        Vector at which f_cost has minimum at the moment
 */
void o_swarm_Draw(GLuint Shader_RGBA, const swarm3D * s, v3D(float) f_min) {

    glEnableVertexAttribArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, o_swarm_VBO);
        glBufferData(GL_ARRAY_BUFFER, s->n * sizeof(v3D(float)), s->x, GL_DYNAMIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

        v4D(float) RGBA= {200/255.0f, 0/255.0f, 0/255.0f, 1.0};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);
        glDrawArrays(GL_POINTS, 0, (GLsizei)s->n);

        GLvoid * pointer; int i;

        /* za minimum */
        v3D(float) min_vertices[5] =  {
            f_min,
            v3D_add(float)(f_min,(v3D(float)){ 0.1f,  0,  0}),
            v3D_add(float)(f_min,(v3D(float)){ 0,  0.1f,  0}),
            v3D_add(float)(f_min,(v3D(float)){ 0,  0,  0.1f}),
            {0,0,0}
        };

        glBindBuffer(GL_ARRAY_BUFFER, o_swarm_VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(min_vertices), min_vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o_swarm_IBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(o_swarm_min_indices), o_swarm_min_indices, GL_STATIC_DRAW);

        glGetVertexAttribPointerv(0,GL_VERTEX_ATTRIB_ARRAY_POINTER,&pointer);

        RGBA= (v4D(float)){0.0, 0.0, 0.0, 0.1};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);

        for (i=0; i<=4; i+=2)
            glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, pointer+i*4);

        /* za granice */

        v3D(float) bound_vertices[8] =  {
            (v3D(float)){s->max_bound.x, s->min_bound.y, s->min_bound.z},
            (v3D(float)){s->max_bound.x, s->max_bound.y, s->min_bound.z},
            (v3D(float)){s->min_bound.x, s->max_bound.y, s->min_bound.z},
            (v3D(float)){s->min_bound.x, s->min_bound.y, s->min_bound.z},
            (v3D(float)){s->max_bound.x, s->min_bound.y, s->max_bound.z},
            (v3D(float)){s->max_bound.x, s->max_bound.y, s->max_bound.z},
            (v3D(float)){s->min_bound.x, s->max_bound.y, s->max_bound.z},
            (v3D(float)){s->min_bound.x, s->min_bound.y, s->max_bound.z}
        };

        glBindBuffer(GL_ARRAY_BUFFER, o_swarm_VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(bound_vertices), bound_vertices, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, o_swarm_IBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(o_swarm_bound_indices), o_swarm_bound_indices, GL_STATIC_DRAW);

        glGetVertexAttribPointerv(0,GL_VERTEX_ATTRIB_ARRAY_POINTER,&pointer);

        RGBA= (v4D(float)){0.0, 0.0, 0.0, 0.1};
        glUniform4fv(Shader_RGBA, 1, &RGBA.x);

        for (i=0; i<24; i+=2)
            glDrawElements(GL_LINES, 2, GL_UNSIGNED_INT, pointer+i*4);

    glDisableVertexAttribArray(0);

}

#endif // O_SWARM_H_INCLUDED
